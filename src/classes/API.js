import axios from 'axios';
import config from '../../config';

class API {
  /** @type {API} */
  static instance = null;

  static initialize() {
    return new API();
  }

  constructor() {
    /** @type {import('axios').AxiosInstance} */
    this.axios = axios.create({
      baseURL: config.express.host
    });

    this.useInterceptors();

    API.instance = this;
  }

  async gamesList() {
    const result = await this.axios.get('/game/list');

    return result;
  }

  async gameJoin(gameId) {
    const result = await this.axios.post('/game/join', {
      gameId
    });

    return result;
  }

  async gameCreate(name) {
    const result = await this.axios.post('/game/create', {
      name
    });

    return result;
  }

  async userPerosnal() {
    const result = await this.axios.get('/user');

    return result;
  }

  async register(regData) {
    const result = await this.axios.post('/register', {
      ...regData
    });

    return result;
  }

  async auth(authData) {
    const result = await this.axios.post('/auth', {
      ...authData
    });

    return result;
  }

  async handshake() {
    const result = await this.axios.get('/handshake');

    return result;
  }

  useInterceptors() {
    this.axios.interceptors.request.use((config) => {
      config.headers.authorization = window.localStorage.getItem('token');
      return config;
    })

    this.axios.interceptors.response.use((response) => {
      if (!response?.data?.success) {
        const reason = response?.data?.data ?? 'неизвестная причина ошибки';

        console.error(`Ошибка запроса: ${response.config.method.toUpperCase()} ${response.config.url}. Причина: ${reason}`);
      }

      return response.data.data;
    }, (error) => {
      console.error(1, error)
    });
  }
}
export default API;