import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'

import API from './classes/API';

API.initialize();

Vue.component('svg-icon', () => import('./components/global/SvgIcon.vue'));

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
