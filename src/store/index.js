import Vue from 'vue'
import Vuex from 'vuex'
import API from '../classes/API'
import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    game: null,
    player: null,
    dialogQueue: [],
    gamesList: []
  },
  getters: {
    dialogQueue: ({ dialogQueue }) => dialogQueue,
  },
  mutations: {
    USER_SET: (state, payload) => state.user = payload,
    GAME_SET: (state, payload) => state.game = payload,
    PLAYER_SET: (state, payload) => state.player = payload,
    GAMES_LIST_SET: (state, payload) => state.gamesList = payload,

    DIALOG_QUEUE: (state, { dialog, force }) => {
      if (force) {
        state.dialogQueue.unshift(dialog);
        return;
      }

      state.dialogQueue.push(dialog);
    },

    DIALOG_CLOSE: (state, { clear }) => {
      if (clear) {
        state.dialogQueue = [];
        return;
      }

      state.dialogQueue.splice(0, 1);
    },
  },
  actions: {
    GAME_JOIN: async ({ commit }, gameId) => {
      const result = await API.instance.gameJoin(gameId);
      if (!result) {
        return;
      }

      commit('GAME_SET', result.game);
      commit('PLAYER_SET', result.player);
    },
    GAME_CREATE: async ({ commit }, name) => {
      const result = await API.instance.gameCreate(name);
      if (!result) {
        return;
      }

      commit('GAME_SET', result.game);
      commit('PLAYER_SET', result.player);
    },
    GAMES_LIST: async ({ commit }) => {
      const result = await API.instance.gamesList();
      if (!result) {
        return;
      }

      commit('GAMES_LIST_SET', result);
    },
    REGISTER: async ({ dispatch }, regData) => {
      const result = await API.instance.register(regData);
      if (!result) {
        return;
      }

      window.localStorage.setItem('token', result);
      dispatch('USER_GET');
      hideModal();
    },
    AUTH: async ({ dispatch }, authData) => {
      const result = await API.instance.auth(authData);
      if (!result) {
        return;
      }

      window.localStorage.setItem('token', result);
      dispatch('USER_GET');
      hideModal();
    },
    USER_GET: async ({ commit }) => {
      const result = await API.instance.userPerosnal();
      if (!result) {
        return;
      }

      commit('USER_SET', result);
    }
  },
  modules: {
  }
})
